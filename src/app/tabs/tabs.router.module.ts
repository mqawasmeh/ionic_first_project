import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
  import { TabsPage } from './tabs.page';
import { HomePagePage } from '../home-page/home-page.page'
import {AboutPage} from '../about/about.page'
import {ConnectPage} from '../connect/connect.page'
const routes: Routes = [
    // {
    //   path: '',
    //   redirectTo: '/tabs/home',
    //   pathMatch: 'full',
    //   // canActivate: [AuthGuard],
    // },
    {
    path: 'tabs',
    component: TabsPage,
    children: [
      {

        path: 'Home',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../home-page/home-page.module').then(m => m.HomePagePageModule)
          }
        ]
      },
      {

        path: 'About',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../About/About.module').then(m => m.AboutPageModule)
          }
        ]
      },
      {

        path: 'Connect',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../connect/connect.module').then(m => m.ConnectPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/Home',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/Home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
