import { Component } from '@angular/core';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  constructor() {}
  Facebook(){
    console.log("True");
    window.open("https://www.facebook.com","_blank");
  }
  Instagram(){
    console.log("True");
    window.open("https://www.instagram.com","_blank");
  }
  Twitter(){
    console.log("True");
    window.open("https://twitter.com","_blank");
  }
  Email(){
    console.log("True");
    window.open("https://mail.google.com","_blank");
  }
}

